import React from 'react'
import Item1 from './Item/Item1'
import Item2 from './Item/Item2'
import Item3 from './Item/Item3'
import Item4 from './Item/Item4'
import Item5 from './Item/Item5'
import Item6 from './Item/Item6'
import './StyleComponent/Style.css'

const Body = () => {
  return (
    <section className='pt-4'>
        <div className="container px-lg-5">
            <div className="row gx-lg-5">
                <Item1/>
                <Item2/>
                <Item3/>
                <Item4/>
                <Item5/>
                <Item6/>
            </div>
        </div>
    </section>
  )
}

export default Body
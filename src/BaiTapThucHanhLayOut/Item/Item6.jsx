import React from 'react'

const Item6 = () => {
  return (
    <div className="col-lg-4 col-xl-4 mb-5">
  <div className="card bg-light border-0 h-100">
    <div className="card-body text-center pt-0 pt-lg-0">
      <div className="feature bg-primary bg-gradient text-white rounded mb-4 mt-n4"><i className="bi bi-patch-check" /></div>
      <h2 className="fs-4 fw-bold"style={{fontSize:"22px"}}>A name you trust</h2>
      <p className="mb-0" style={{fontSize:"16px"}}>Start Bootstrap has been the leader in free Bootstrap templates since 2013!</p>
    </div>
  </div>
</div>

  )
}

export default Item6
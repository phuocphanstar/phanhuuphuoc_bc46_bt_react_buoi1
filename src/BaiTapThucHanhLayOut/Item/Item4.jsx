import React from 'react'

const Item4 = () => {
  return (
    <div className="col-lg-4 col-xxl-4 mb-5">
  <div className="card bg-light border-0 h-100">
    <div className="card-body text-center pt-0 pt-lg-0">
      <div className="feature bg-primary bg-gradient text-white rounded mb-4 mt-n4"><i className="bi bi-bootstrap" /></div>
      <h2 className="fs-4 fw-bold" style={{fontSize:"22px"}}>Feature boxes</h2>
      <p className="mb-0" style={{fontSize:"16px"}}>We've created some custom feature boxes using Bootstrap icons!</p>
    </div>
  </div>
</div>

  )
}

export default Item4
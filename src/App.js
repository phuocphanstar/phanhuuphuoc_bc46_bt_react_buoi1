import './App.css';
import Banner from './BaiTapThucHanhLayOut/Banner';
import Body from './BaiTapThucHanhLayOut/Body';
import Footer from './BaiTapThucHanhLayOut/Footer';
import Header from './BaiTapThucHanhLayOut/Header';
import 'bootstrap-icons/font/bootstrap-icons.css';

function App() {
  return (
    <div>
      <Header/>
      <Banner/>
      <Body/>
      <Footer/>
    </div>
  );
}

export default App;
